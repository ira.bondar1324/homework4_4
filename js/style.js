"use strict";

const URL=`https://ajax.test-danit.com/api/swapi/films`;
const root=document.querySelector("#root");

class Request{
    createRequest(url){
        return fetch(url).then(response=>response.json()).catch(e=>console.log("Error ",e));
    }
}

class StarWars{
    constructor(){
        this.request=new Request();
    }
    getFilms(){
        const films = this.request;
        return films.createRequest(URL).then(data=>{
            data.map((el)=>{
                this.render(el);
            })
        });   
    };
    render(film){
        const{episodeId,name,openingCrawl}=film;
        let filmInfo=createElement("ul");
        const filmName=createElement("li",`${name}`);
        const filmId=createElement("li",`${episodeId}`);
        const filmDescrip=createElement("li",`${openingCrawl}`);
        const listNames=createElement("ul");
        const {characters}=film;
        characters.map(e=>{
            const nameLi=createElement("li");
            const names=this.request;
            names.createRequest(e).then(data=>{
                nameLi.innerHTML=data.name;
                return nameLi;
            });
            Promise.all([names]).then(()=>{
                listNames.append(nameLi);
            });
        });
        const filmCharacters=createElement("li");
        filmCharacters.append(listNames);
        filmInfo.append(filmName,filmCharacters,filmId,filmDescrip);
        root.append(filmInfo);
        return root;
    }
}

const starwars=new StarWars();
starwars.getFilms();


function createElement (element, innerHTML=null){
    let el=document.createElement(element);
    el.innerHTML=innerHTML; 
    return el;
}